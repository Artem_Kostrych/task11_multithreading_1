package com.kostrych;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
    private List<Integer> fibonacciNumbers;

    public Fibonacci(int n) {
        fibonacciNumbers = new ArrayList(n);
        fibonacciNumbers.add(0);
        for (int i = 1; i < n; i++) {
            fibonacciNumbers.add(i+fibonacciNumbers.get(i-1));
        }
    }
}
